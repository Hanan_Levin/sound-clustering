import numpy as np
import librosa
import os
import fnmatch
import re

# constants for printing titles.
MARGIN = "-" * 35
TITLE = "{margin} {title} {margin}"


def get_labeled_raw_data(sound_bank_path, audio_sample_amount):
    """
    sample audio_sample_amount samples of '.mp3' files from SOUND_BANK_PATH.
    construct a label for each (the sound samples name) and sort them in alphabetical order.
    :return: (samples, labels)
    """
    audio_files = []
    instruments = set()
    for root, dirs, files in os.walk(sound_bank_path):
        for filename in fnmatch.filter(files, '*.mp3'):
            instruments.add(filename.split('_')[0])  # directory always has the name of the instrument.
            audio_files.append("{root}/{filename}".format(root=root, filename=filename))
    print("found {num} audio files in {path}".format(num=len(audio_files), path=sound_bank_path))
    audio_files = filter_audio_files(audio_files)
    print("Number of filtered files is {num}".format(num=len(audio_files)))
    samples = select_equal_samples_of_instruments(audio_files, instruments, audio_sample_amount)
    labels = []
    print(TITLE.format(margin=MARGIN, title=" Data labels"))
    for file in samples:
        label = re.findall(r"/[^/]*", file)[2][1:]
        label = os.path.splitext(label)[0]  # remove '.mp3' extension
        labels.append(label)
        print(label)

    # sort samples alphabetically
    sort_indices = np.argsort(labels)
    labels = np.array(labels)[sort_indices]
    samples = np.array(samples)[sort_indices]
    return samples, labels


def filter_audio_files(audio_files):
    """
    filter the audio files for specific characteristics based on their name (pitch, length, dynamics...)
    """

    audio_files = [file for file in audio_files if
                   "normal" in file and "_1_" in file or ("very-long" in file and "normal" in file)]
    return audio_files


def select_equal_samples_of_instruments(audio_files, instruments, audio_sample_amount):
    """
    return an equal amount of samples from each instrument in the given labeled audio files.
    the amount of samples will be the minimum between the available samples per instrument * num instruments,
    and the requested samples.
    :param audio_files: all the samples
    :param instruments: list of all available instruments
    :param audio_sample_amount: the requested amount
    :return: equally samples list.
    """
    instrument_dict = {instrument: [] for instrument in instruments}
    for audio in audio_files:
        instrument = (audio.split('_')[0]).split('/')[-1]
        instrument_dict[instrument].append(audio)
    samples_count = [len(instrument_dict[instrument]) for instrument in instrument_dict.keys()]
    minimum_appearances = np.min(samples_count)
    sample_amount = int(min(minimum_appearances * len(instruments), audio_sample_amount/len(instruments)))
    audio_samples = []
    for instrument in instrument_dict.keys():
        # samples = np.random.choice(instrument_dict[instrument], sample_amount, replace=False)
        samples = instrument_dict[instrument][:sample_amount]
        audio_samples.extend(samples)
    return audio_samples


