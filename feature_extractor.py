from sklearn.preprocessing import StandardScaler
import numpy as np
import librosa  # sound lib
import pickle


# feature embedding types.
MFCC = 'MFCC'
RAW_AUDIO = 'RAW_AUDIO'
ENCODER = 'ENCODER'

SAMPLING_RATE = 44100  # standard for audio recordings.

def get_MFCC_features(sample):
    """
    get the MFCC features of an audio sample.
    """

    n_mels = 128  # Number of Mel bands
    n_mfcc = 13  # MFCC coefficients. It is standard not to take higher frequencies.
    S = librosa.feature.melspectrogram(sample, sr=SAMPLING_RATE, n_mels=n_mels)
    mfcc = librosa.feature.mfcc(S=librosa.power_to_db(S), n_mfcc=n_mfcc)[1:]  # discard first coef. has no useful info.
    feature_vector = np.mean(mfcc, axis=1)
    return feature_vector


def get_Encoder_features(sample):
    """
    get features from the encoder in google's nsynth Autoencoder.
    :param sample:
    :return:
    """
    from magenta.models.nsynth import utils
    from magenta.models.nsynth.wavenet import fastgen
    sr = 16000
    audio = utils.load_audio(sample, sample_length=40000, sr=sr)
    sample_length = audio.shape[0]
    encodings = fastgen.encode(audio, './wavenet-ckpt/wavenet-ckpt/model.ckpt-200000', sample_length)

    return encodings, encodings.flatten()

def extract_features(files, feature_type=MFCC):
    """
    load up the files and extract feature representation of each.
    :param files: the audio files to process.
    :param feature_type: the type of feature extraction wanted.
    :return: a numpy array of the feature vectors extracted.
    """
    feature_vectors = []
    if feature_type == RAW_AUDIO:  # raw audio feats. The simple normalized wave forms.
        for i, file_path in enumerate(files):
            print("Getting sample {idx} out of {total}: {path}".format(idx=i + 1, total=len(files), path=file_path))
            feat, _ = librosa.load(file_path, sr=SAMPLING_RATE)
            feat /= feat.max()
            print("encoding {i} is of shape {e} with size of {s}".format(i=i, e=feat.shape, s=feat.size))
            feature_vectors.append(feat)
        lengths = [len(f) for f in feature_vectors]
        min_length = np.min(lengths)
        normal_length = [f[:min_length] for f in feature_vectors]
        return np.array(normal_length)

    # Auto encoder feats

    if feature_type == ENCODER:
        # the next imports are only used for the autoencoder. don't import it other wise, it takes a while to load.
        from magenta.models.nsynth import utils
        from magenta.models.nsynth.wavenet import fastgen
        encodings = []

        for i, file_path in enumerate(files):
            print("Getting sample {idx} out of {total}: {path}".format(idx=i + 1, total=len(files), path=file_path))
            encoding, feat = get_Encoder_features(file_path)
            print("encoding {i} is of shape {e} with size of {s}".format(i=i, e=feat.shape, s=feat.size))
            encodings.append(encoding)
            feature_vectors.append(feat)
        with open('processed_encodings_{feature_type}.pkl'.format(feature_type=feature_type), 'wb') as f:
            pickle.dump(encodings, f)
        lengths = [len(f) for f in feature_vectors]
        min_length = np.min(lengths)
        normal_length = [f[:min_length] for f in feature_vectors]
        return np.array(normal_length)

    else:  # MFCC feats
        for i, file_path in enumerate(files):
            print("Getting sample {idx} out of {total}: {path}".format(idx=i + 1, total=len(files), path=file_path))
            try:
                y, _ = librosa.load(file_path, sr=SAMPLING_RATE)
                y /= y.max()  # normalize audio amplitude.
                if len(y) < 2:
                    print("Error loading {fname}".format(fname=file_path) % file_path)
                    continue
                features = get_MFCC_features(y)
                feature_vectors.append(features)
            except Exception as e:
                print("Error loading {path}. Error: {e}".format(path=file_path, e=e))

        print("\n")
        scaler = StandardScaler()
        scaled_feature_vectors = scaler.fit_transform(np.array(feature_vectors))
        print("Feature vectors shape:", scaled_feature_vectors.shape)
        return np.array(feature_vectors)

def normalize_by_feature(data):
    """
    normalizes every feature of the data
    unlike 'normalize_data' which normalizes the whole matrix
    :param data: data matrix
    :return: data matrix with every feature normalized
    """
    new_data = data.copy()
    for feature in range(data.shape[1]):
        new_data[:,feature] = (data[:,feature] - np.min(data[:,feature]))\
                               /(np.max(data[:,feature] - np.min(data[:,feature])))

    return new_data