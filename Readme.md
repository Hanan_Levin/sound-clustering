# Sound Palette - audio sample clustering

*A team project made for the course 'Needle in a data haystack' at the Hebrew University.* 

For a digital musician choosing the right musical sample to use can be a daunting search task.
In this project we experimented with clustering methods on different feature selections for the purpose of clustering sound banks.
We used a sound bank of traditional instruments which gave us a sturdy 'ground truth' to measure our clustering against. 
## Explored sound encodings:
*	MFCC
*	Autoencoder

## Explored clustering methods:
*	K-Means
*	Agglomerative clustering 
*	Spectral clustering



Some results can be seen in the confusion matrix image, further details can be found in the accompanying [report](https://gitlab.com/Hanan_Levin/sound-clustering/-/blob/master/report_and_images/SoundPalette.pdf).
![](/report_and_images/confusion_matrix.PNG)
