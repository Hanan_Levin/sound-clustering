from scipy.cluster.hierarchy import dendrogram, linkage, cut_tree
from scipy.cluster.vq import kmeans, vq
from sklearn.cluster import SpectralClustering, KMeans, AgglomerativeClustering
from sklearn.metrics import silhouette_score
from scipy.spatial import distance
from matplotlib import pyplot as plt
from scipy.spatial.distance import cdist
import pandas as pd
import numpy as np

# constants for printing titles.
MARGIN = "-" * 35
TITLE = "{margin} {title} {margin}"


def agglomerative_clustering(data, labels, num_clusters):
    model = AgglomerativeClustering(n_clusters=num_clusters)
    preds = model.fit_predict(data)

    # represent the cluster in as a dictionary with keys as cluster index and values as lists of labels in each cluster.
    clust_dict = {i: [] for i in range(num_clusters)}
    print(TITLE.format(margin=MARGIN, title=" Spectral clustering results"))
    for clust_num in range(num_clusters):
        labels = np.array(labels)
        clust_dict[clust_num] = labels[np.where(model.labels_ == clust_num)]
        print("Cluster number {num}".format(num=clust_num))
        for data_point in clust_dict[clust_num]:
            print(data_point)
        print("********************")

    linked = linkage(data, 'single')
    plt.figure(figsize=(10, 7))
    dendrogram(linked, show_contracted=False,
               leaf_rotation=90, p=num_clusters, truncate_mode="lastp", show_leaf_counts=True)
    plt.title("Agglomarative clustering dendogram")
    plt.tight_layout()
    plt.show()
    return clust_dict


def spectral_clustering(data, labels, num_clusters):
    """
    perform spectral clustering on the data.
    """
    spectral = SpectralClustering(n_clusters=num_clusters, eigen_solver='arpack', affinity="nearest_neighbors")
    spectral.fit_predict(data)

    # represent the cluster in as a dictionary with keys as cluster index and values as lists of labels in each cluster.
    clust_dict = {i: [] for i in range(num_clusters)}
    print(TITLE.format(margin=MARGIN, title=" Spectral clustering results"))
    for clust_num in range(num_clusters):
        labels = np.array(labels)
        clust_dict[clust_num] = labels[np.where(spectral.labels_ == clust_num)]
        print("Cluster number {num}".format(num=clust_num))
        for data_point in clust_dict[clust_num]:
            print(data_point)
        print("********************")
    return clust_dict


def get_k_nearest_neighbors(data, indices, labels, k):
    """
    Get the k nearest neighbors of each of the samples (the data in the indices positions).
    Print out the the results with of each point and its distance from the original.
    """

    data = np.array(data)
    similarity_matrix = distance.cdist(data, data, 'euclidean')
    print(TITLE.format(margin=MARGIN, title=" KNN results on sampled audio files"))
    for idx in indices:
        print("Audio sample is: {data_point}".format(data_point=labels[idx]))
        k_nearest_indices = np.argsort(similarity_matrix[idx])[1:k + 1]  # no need to take 0, it will always be self.
        k_nearest = labels[k_nearest_indices]
        for i in range(len(k_nearest)):
            neighbor = k_nearest[i]
            print("Neighbor: {n} \n\t Distace: {d}".format(n=neighbor,
                                                           d=similarity_matrix[idx][k_nearest_indices[i]]))
        print("********************")


def guess_k(data, min_k, max_k, num_iter):
    """
    uses the elbow method and the best average silhouette methods to find a good guess for the number of clusters
    :param data: the data we are clustering
    :param min_k: minimal k to alow
    :param max_k: maximal k to allow
    :param n_init: number of kmeans runs on each cluster count
    :return: the guess for a k
    """
    k_list = []
    for i in range(num_iter):
        avg_sil = []
        distortions = [-np.inf]
        K = range(min_k, max_k + 1)
        for k in K:
            model = KMeans(n_clusters=k)

            pred = model.fit_predict(data)
            distortions.append(sum(np.min(cdist(data, model.cluster_centers_, 'euclidean'), axis=1)) / data.shape[0])
            avg_sil.append(silhouette_score(data, pred, metric="euclidean"))

        # plot elbow and silhouette
        # fig, ax1 = plt.subplots()
        # color = 'tab:red'
        # ax1.set_xlabel("Number of Clusters")
        # ax1.set_ylabel('Distortion', color=color)
        # ax1.plot(K, distortions[1:], 'bx-', color=color)
        # ax1.tick_params(axis='y', labelcolor=color)
        # ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
        # color = 'tab:blue'
        # ax2.set_ylabel('Average Silhouette', color=color)  # we already handled the x-label with ax1
        # ax2.plot(K, avg_sil, 'bx-', color=color)
        # ax2.tick_params(axis='y', labelcolor=color)
        # fig.tight_layout()  # otherwise the right y-label is slightly clipped
        # plt.title("Choosing number of cluster with autoencoder features")
        # plt.plot(K, avg_sil, 'bx-')
        # plt.show()

        elbow_slope = [(distortions[i + 1] - distortions[i]) - (distortions[i] - distortions[i - 1]) for i in
                       range(1, len(distortions) - 1)]
        elbow_slope.append(-np.inf)
        k_by_elbow = np.flip(np.argsort(elbow_slope))
        k_by_silhouette = np.flip(np.argsort(avg_sil))
        index_sum = [i + 1 + np.where(k_by_silhouette == k_by_elbow[i])[0] for i in range(len(k_by_elbow))]
        guess = k_by_elbow[np.argmin(index_sum)] + min_k
        k_list.append(guess)
        # print("guess is {g}".format(g=guess))
    return int(np.mean(k_list))


def k_means(data, labels, num_clusters):
    """
    performs k means clustering on the data
    :param data: data to cluster
    :param num_clusters: number of cluster to create
    :param n_init: number of k means runs to perform before averaging the results
    :return:
    """
    model = KMeans(n_clusters=num_clusters)
    preds = model.fit_predict(data)

    clust_dict = {i: [] for i in range(num_clusters)}
    for i in range(len(data)):
        clust_dict[preds[i]].append(labels[i])

    # print the results
    print(TITLE.format(margin=MARGIN, title=" K-Means results"))
    for clust_num in clust_dict:
        print("Cluster number {num}".format(num=clust_num))
        for data_point in clust_dict[clust_num]:
            print(data_point)
        print("********************")
    return clust_dict
