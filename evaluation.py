from collections import Counter
import numpy as np
import os
import fnmatch
import librosa
from data_collection import get_labeled_raw_data
from feature_extractor import extract_features, normalize_by_feature
from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix
import time
import matplotlib.pyplot as plt
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.colors as colors
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import seaborn as sn
import pickle

import clustering

SAMPLING_RATE = 44100  # standard for audio recordings.

# constants for visualization
Z_LABEL = 'Z Label'
Y_LABEL = 'Y Label'
X_LABEL = 'X Label'
# COLOR_LIST = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'indigo', 'aqua', 'gold']
SHAPE_LIST = ["x", "D", "h", "s", "3", ".", "^", "*"]

COLOR_LIST = list(colors._colors_full_map.values())


def compare_embedding_techniques(embedding_list, sound_bank_path, n, pickled_encoder_path='processed_data_ENCODER.pkl'):
    # n is the size of X. n must be much larger than m. see more in the link in the description.
    m = int(0.1 * n)  # heuristic for m  (we need m<<n)
    X, _ = get_labeled_raw_data(sound_bank_path, audio_sample_amount=n)
    hopkins_results = np.zeros(len(embedding_list))
    for i, embedding_type in enumerate(embedding_list):
        pickled_encoder_data = None
        if embedding_type is 'ENCODER':
            with open(pickled_encoder_path, 'rb') as f:
                pickled_encoder_data = pickle.load(f)
        hopkins_results[i] = hopkins_statistics(X, m, embedding_type, sound_bank_path, pickled_encoder_data, n)

    colors = ['r', 'g', 'b']
    y_pos = np.arange(len(embedding_list))
    plt.barh(y_pos, hopkins_results, align='center', alpha=0.5, color=colors[:len(embedding_list)])
    plt.yticks(y_pos, embedding_list, rotation=30)
    plt.xlabel('Hopkins statistics')
    plt.title('Embedding type clusterability comparison')
    plt.show()


def hopkins_statistics(X, m, featutre_type, sound_bank_path, pickled_encoder_data, n=300):
    """
    measure the of cluster tendency of the embedded data given using hopkins statistics.
    notice that the embedding must be normalized to values in the range 0-1 for this result to be meaningful.
    see more here: https://en.wikipedia.org/wiki/Hopkins_statistic
    :param featutre_type: the type of embedding we wish to calculate hopkins statistics on.
    :return: a number in the range 0-1 where 1 is a means high cluster tendency.
    """
    if featutre_type == 'ENCODER':  # encoder data takes too long to run, so we take a pre computed pickled version.
        embedded_X = pickled_encoder_data
    else:
        embedded_X = extract_features(X, featutre_type)  # the normalized embedding
    samples_idx = np.random.choice(np.arange(len(embedded_X)), m, replace=False)
    embedded_sampels = embedded_X[samples_idx]

    d = embedded_X.shape[1]  # dimension of the embedding
    Y = np.random.rand(m, d)
    u = np.zeros(m)  # distance of embdded_X_i and Y_i from closest point in X
    w = np.zeros(m)
    for i in range(m):
        u[i] = np.min([(Y[i] - x) for x in embedded_X])
        w[i] = np.min([(embedded_sampels[i] - x) for x in embedded_X])
    H = np.sum(u) / (np.sum(u) + np.sum(w))
    return H


def purity_evaluation(clustered_data):
    """
    compute the purity evaluation for the clustered data. meaning: the extent to which clusters contain a single class.
    :return: a number in the range 0-1 where 1 is a perfect score (each cluster contains only a single class)
    """
    pure_count = 0
    total_count = 0

    for clust_idx in clustered_data:
        clust_labels = clustered_data[clust_idx]
        instruments = [label.split('_')[0] for label in clust_labels]
        b = Counter(instruments)
        most_common, count = b.most_common(1)[0][0], b.most_common(1)[0][1]
        print("The most common instrument in cluster {idx} is {m_c}".format(idx=clust_idx, m_c=most_common))
        pure_count += count
        total_count += len(clustered_data[clust_idx])

    return float(pure_count) / float(total_count)


def sonifi_cluster(clust_dict, sound_bank_path):
    """
    give a sonic representation to the given cluster.
    This is done by using autoencoder. We all the sounds in a given cluster, interpolate the sounds in each cluster,
    and then decode the resulting interpolation.
    The files are then saved with the name of the most frequent instrument in that cluster, and the cluster index.
    :param clust_dict: a dictionary representing a cluster. each key is an index of a cluster, and the values are the
    names of the sound files in that cluster.
    """
    # the next imports are only used for the autoencoder. don't import it other wise, it takes a while to load.
    from magenta.models.nsynth import utils
    from magenta.models.nsynth.wavenet import fastgen
    sr = 16000
    for idx in clust_dict.keys():
        file_paths = []
        instruments = [label.split('_')[0] for label in clust_dict[idx]]
        b = Counter(instruments)
        most_common_instrument, _ = b.most_common(1)[0][0], b.most_common(1)[0][1]
        files_paths = []
        for label in clust_dict[idx]:
            file_path = find_file_from_label(label, sound_bank_path)
            file_paths.append(file_path)
        data = []
        sample_lengths = []
        for path in file_paths:
            audio = utils.load_audio(path, sample_length=40000, sr=sr)
            data.append(audio)
            sample_lengths.append(audio.shape[0])
        sample_length = np.min(sample_lengths)
        i = 0
        cluster_encodings = []
        for audio in data:
            i += 1
            audio = audio[:sample_length]
            print(audio.shape[0])
            start = time.time()
            cluster_encodings.append(
                fastgen.encode(audio, './wavenet-ckpt/wavenet-ckpt/model.ckpt-200000', sample_length))
            end = time.time()
            print("encoding sample" + str(i) + " took" + str(end - start) + "seconds")

        sum = cluster_encodings[0]
        for i in range(len(cluster_encodings) - 1):
            sum += cluster_encodings[i + 1]
        interpolated = sum / float(len(cluster_encodings))
        print(interpolated.shape)
        start = time.time()
        fastgen.synthesize(interpolated, save_paths=[
            'gen_cluster{idx}_prominent_sound_{main}'.format(idx=idx, main=most_common_instrument)],
                           checkpoint_path="./wavenet-ckpt/wavenet-ckpt/model.ckpt-200000",
                           samples_per_save=sample_length)
        end = time.time()
        print("decoding took" + str(end - start) + "seconds")


def find_file_from_label(label, sound_bank_path):
    """
    trasnlate a label of an sound recording to the file it came from.
    :param label: the label of the audio file. all lables start with the instrument name followed by _. the labels are
    a part of the file name (following the
    sound banks labeling convention).
    :param sound_bank_path: path in which the files are located. the sound bank is organized in separate directories for
    separate instruments.
    :return: the relative path to the audio file
    """
    audio_files = []
    for root, dirs, files in os.walk(sound_bank_path):
        for filename in fnmatch.filter(files, '*.mp3'):
            audio_files.append("{root}/{filename}".format(root=root, filename=filename))
    instrument = label.split('_')[0]
    instrument_dir = instrument.replace('-', ' ')
    instrument_dir_path = "{root}{instrument}".format(root=sound_bank_path, instrument=instrument_dir)
    audio_file_name = ''
    for root, dirs, files in os.walk(sound_bank_path):
        for filename in fnmatch.filter(files, '*.mp3'):
            if label in filename:
                audio_file_name = filename
                break
    if not audio_file_name:
        print("ERROR no file with label {l} found in the sound bank".format(l=label))
    file_path = "{instrument}/{file}".format(instrument=instrument_dir_path, file=audio_file_name)
    return file_path


def visualization_preprocessor(data, labels, num_dims, feature_norm=False):
    """
    preprocess the data for visualization, creats a patch for legend , and build a dataframe of the data for the
    vizualization
    LIMITIS 1. number of tools cannot exceed len SHAPE_LIST,
    LIMITIS 2. number of cluster  cannot exceed len COLOR_LIST ,
    :param data: the data vectors in the MFCC space
    :param labels: the label's of the data point
    :param num_dims: number of dimension's (2 or 3)
    :param feature_norm: bool normalize the whole data or by feature

    :return: transformed= dataframe with all data  ( index= lable, X,Y,Z,type,shape),
             patch_list= patch list for  all shapes for LEGEND
    """
    type_vec = [l.split("_")[0] for l in labels]
    type_name = list(set(type_vec))  # get all unique tools name
    assert (len(type_name) <= len(SHAPE_LIST))
    norm_data = normalize_by_feature(data)
    pca = PCA(n_components=num_dims)
    # transformed = pd.DataFrame(pca.fit_transform(norm_data))
    transformed = pd.DataFrame(pca.fit_transform(norm_data), index=labels)

    transformed['type'] = pd.Series(type_vec, index=transformed.index)
    # create a dictionary for the shapes to plot (shape name: visualization)
    tool_shape_map = SHAPE_LIST[:len(type_name)]
    shape_dict = dict(zip(type_name, tool_shape_map))
    shape_vec = [shape_dict[i] for i in type_vec]
    transformed['shape'] = pd.Series(shape_vec, index=transformed.index)
    patch_list = []
    for shape in shape_dict:
        data_key = mlines.Line2D([], [], marker=shape_dict[shape], color=None, label=shape, linestyle='None')
        patch_list.append(data_key)
    return transformed, patch_list


def pca_visualization(data, labels, cluster_dict, num_dims, feature_norm=False):
    """
    uses pca to visualize the data in 3d
    and creates a 3d plot , where each cluster has a color and each tool has a shape
    LIMITIS 1. number of tools cannot exceed len SHAPE LIST,
    LIMITIS 2. number of cluster  cannot exceed len COLOR_LIST ,
    :param data: the data vectors in the MFCC space
    :param labels: the labeles of the data point
    :param num_dims: number of dimenstions (2 or 3)
    :param feature_norm: bool normalize the whole data or by feature
    """
    assert (len(cluster_dict) <= len(COLOR_LIST))
    transformed, patch_list = visualization_preprocessor(data, labels, num_dims, feature_norm)
    if num_dims == 3:
        transformed.columns = [X_LABEL, Y_LABEL, Z_LABEL, "type", "shape"]
        fig = plt.figure()
        ax = Axes3D(fig)
        for cluster in range(len(cluster_dict)):
            # add colors to legend
            data_key = mpatches.Patch(color=COLOR_LIST[cluster], label="cluster{0}".format(cluster))
            # patch_list.append(data_key)

            for sound in cluster_dict[cluster]:
                df = transformed.loc[sound]
                xs = df[X_LABEL]
                ys = df[Y_LABEL]
                zs = df[Z_LABEL]
                ax.scatter(xs, ys, zs, color=COLOR_LIST[cluster*10], marker=df['shape'])
        # set 3d labels
        # ax.set_xlabel(X_LABEL)
        # ax.set_ylabel(Y_LABEL)
        # ax.set_zlabel(Z_LABEL)
        ax.legend()

        plt.legend(loc="best", handles=patch_list)

        ax.set_title("3d PCA, 8 clusters\nspectral clustering on autoencoder features")
        plt.show()
        return
    else:  # for future visualization
        pass


def plot_confusion(clustered_data):
    """
    creates and plots confusion matrix acoording to the clustering results
    :param clustered_data: dictionary of cluster tags and the instruments that were assigned to them
    :return:
    """
    truth = []
    tag = []
    cluster_names = []
    instruments_names = ["oboe", "viola", "flute", "trumpet", "double-bass", "mandolin", "saxophone", "guitar"]

    for clust_idx in clustered_data:
        clust_labels = clustered_data[clust_idx]
        instruments = [label.split('_')[0] for label in clust_labels]
        b = Counter(instruments)
        most_common, count = b.most_common(1)[0][0], b.most_common(1)[0][1]
        for i in range(len(instruments)):
            truth.append(instruments[i])
            tag.append(most_common)

    conf_matrix = confusion_matrix(truth, tag, instruments_names)
    print(conf_matrix)
    df_cm = pd.DataFrame(conf_matrix, index=[instrument for instrument in instruments_names],
                         columns=[instrument for instrument in instruments_names])
    plt.figure(figsize=(10, 7))
    sn.heatmap(df_cm, annot=True)
    plt.title("Confusion matrix, 8 clusters\nspectral clustering on autoencoder features")
    plt.show()


def compare_clustering_techniques(autoencoding, mfcc, encoder_labels, mfcc_labels):
    """
    creates bar charts compating different clustering methods using the purity function
    :param autoencoding: data matrix from the autoencoder
    :param mfcc: data matrix of mfcc features

    assumes normalized data
    :return:
    """
    # k_mfcc = clustering.guess_k(mfcc,2,25,10)
    k_mfcc = 11
    k_means_mfcc = clustering.k_means(mfcc, mfcc_labels, k_mfcc)
    k_means_mfcc_score = purity_evaluation(k_means_mfcc)

    positions = [1, 2, 3]

    spectral_mfcc = clustering.spectral_clustering(mfcc, mfcc_labels, k_mfcc)
    spectral_mfcc_score = purity_evaluation(spectral_mfcc)

    agg_mfcc = clustering.agglomerative_clustering(mfcc, mfcc_labels, k_mfcc)
    agg_mfcc_score = purity_evaluation(agg_mfcc)

    models = ["K Means", "Spectral", "Agglomerative"]
    mfcc_scores = [k_means_mfcc_score, spectral_mfcc_score, agg_mfcc_score]

    print(mfcc_scores)

    plt.title("Purity score with different MFCC clustering\n number of clusters = {k}".format(k=k_mfcc))
    plt.bar(positions, mfcc_scores, align='center', alpha=0.5)
    plt.xticks(positions, models)
    plt.ylabel("Purity Score")
    plt.show()

    k_ae = 11
    # k_ae = clustering.guess_k(autoencoding,2,25,10)
    k_means_ae = clustering.k_means(autoencoding, encoder_labels, k_ae)
    k_means_ae_score = purity_evaluation(k_means_ae)

    positions = [1, 2, 3]

    spectral_ae = clustering.spectral_clustering(autoencoding, encoder_labels, k_ae)
    spectral_ae_score = purity_evaluation(spectral_ae)

    agg_ae = clustering.agglomerative_clustering(autoencoding, encoder_labels, k_ae)
    agg_ae_score = purity_evaluation(agg_ae)

    models = ["K Means", "Spectral", "Agglomerative"]
    ae_scores = [k_means_ae_score, spectral_ae_score, agg_ae_score]

    plt.title("Purity score with different AUTOENCODER clustering\n number of clusters = {k}".format(k=k_ae))
    plt.bar(positions, ae_scores, alpha=0.5)
    plt.xticks(positions, models)
    plt.ylabel("Purity Score")
    plt.show()
