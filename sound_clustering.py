import numpy as np
import argparse
import clustering
import feature_extractor as feat_ex
import evaluation as eval
import argparse
from feature_extractor import RAW_AUDIO, ENCODER, MFCC
import data_collection
import pickle

SOUND_BANK_PATH = './soundBank/'
SAMPLING_RATE = 44100  # standard for audio recordings.


def main(path, audio_sample_amount, feature_type, pickled_data, pickled_labels, save_data, compare=0):
    if pickled_data:  # get data from the pickled input
        with open(pickled_data, 'rb') as f:
            data = pickle.load(f)
        with open(pickled_labels, 'rb') as f:
            labels = pickle.load(f)

    else:  # get data
        sound_files, labels = data_collection.get_labeled_raw_data(path, audio_sample_amount)
        data = feat_ex.extract_features(sound_files, feature_type)
        data = feat_ex.normalize_by_feature(data)
        if save_data:
            with open('processed_data_{feature_type}.pkl'.format(feature_type=feature_type), 'wb') as f:
                pickle.dump(data, f)
            with open('processed_labels_{feature_type}.pkl'.format(feature_type=feature_type), 'wb') as f:
                pickle.dump(labels, f)

    k = clustering.guess_k(feat_ex.normalize_by_feature(data), 2, 25, num_iter=10)
    spectral_clusters = clustering.spectral_clustering(data, labels, k)
    eval.plot_confusion(spectral_clusters)
    clustering.guess_k(data, 3, 25, 1)
    print("The evaluation of spectral clustering is {eval}".format(eval=eval.purity_evaluation(spectral_clusters)))
    eval.pca_visualization(data, labels, spectral_clusters, 3)

    if compare:
        with open('processed_data_ENCODER.pkl', 'rb') as f:
            encoder_feats = pickle.load(f)
        with open('processed_labels_ENCODER.pkl', 'rb') as f:
            encoder_labels = pickle.load(f)
        with open('processed_data_MFCC.pkl', 'rb') as f:
            mfcc_feats = pickle.load(f)
        with open('processed_labels_MFCC.pkl', 'rb') as f:
            mfcc_labels = pickle.load(f)
        eval.compare_clustering_techniques(encoder_feats, mfcc_feats, encoder_labels, mfcc_labels)
    eval.compare_embedding_techniques([RAW_AUDIO, MFCC, ENCODER], SOUND_BANK_PATH, 300)

    eval.sonifi_cluster(spectral_clusters, SOUND_BANK_PATH)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--sound_bank_path', '-path', type=str, default=SOUND_BANK_PATH,
                        help='relative path to the sound bank')
    parser.add_argument('--sample_amount', '-sa', type=int, default=300,
                        help='amount of audio samples from the sound bank to be used for the clustering')
    parser.add_argument('--feature_extraction_type', '-ft', type=str, default=MFCC,
                        help='the type of feature extraction method to use. Should be one of:\nRAW_AUDIO\nMFCC\nENCODER\
                        otherwise the MFCC will be used automatically')
    parser.add_argument('--pickled_data_features_path', '-pd', type=str, default='processed_data_ENCODER.pkl',
                        help='path for the feature extracted data with the corresponding features')
    parser.add_argument('--pickled_labels', '-pl', type=str, default='processed_labels_ENCODER.pkl',
                        help='path for the feature extracted data labels')
    parser.add_argument('--save_processed_data', '-spd', type=int, default=0,
                        help='if 1, save the extracted features into a pickled list')
    parser.add_argument('--compare_clustering_techniques', '-cct', type=int, default=0,
                        help='plot a comparison of the clustering techniques')
    args = parser.parse_args()
    main(path=args.sound_bank_path, audio_sample_amount=args.sample_amount, feature_type=args.feature_extraction_type,
         pickled_data=args.pickled_data_features_path, pickled_labels=args.pickled_labels,
         save_data=args.save_processed_data, compare=args.compare_clustering_techniques)
